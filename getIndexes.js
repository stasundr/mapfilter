const _ = require('highland')
const fs = require('fs')

const stream = fs.createReadStream('./data', 'utf8')
const names = ['petya', 'vasya']

function generateIndexesArray(line) {
  let indexes = []
  let array = line.split(/\s+/)

  array.forEach((el, index) => {
    if (names.indexOf(el) >= 0) indexes.push(index)
  })

  console.log(indexes)
}

_(stream)
  .split()
  .drop(6)
  .take(1)
  .each(generateIndexesArray)
