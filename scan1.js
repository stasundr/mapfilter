const fs = require('fs')
const highland = require('highland')

const stream = fs.createReadStream('scan1_data', 'utf8')

highland(stream)
  .split()
  .filter(line => line !== '')
  .scan(
    { totalMoney: 0, currentYear: null, currentYearMoney: 0 }, // Начальное состояние memo
    (memo, line) => {
      if (/Зарплата/.test(line)) {
        if (memo.currentYear) {
          console.log(
            `За ${memo.currentYear} заработано ${memo.currentYearMoney}$`
          )
        }

        memo.currentYear = line.split(' ')[2]
        memo.currentYearMoney = 0
      } else {
        const salary = parseInt(line.split(' ')[1])

        memo.totalMoney += salary
        memo.currentYearMoney += salary
      }

      return memo
    }
  )
  .each(memo => {
    if (memo.currentYear) {
      console.log(
        `С начала ${memo.currentYear} заработано ${memo.currentYearMoney}$`
      )
    }

    console.log(`Всего заработано: ${memo.totalMoney}!`)
  })
