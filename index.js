const _ = require('highland')
const fs = require('fs')

const stream = fs.createReadStream('./data', 'utf8')
const names = ['petya', 'vasya']
const indexes = require('./indexes.json')

function filterEmptyLine(line) {
  if (line === '') return false
  else return true
}

function transformStringToArray(line) {
  return line.split(/\s+/)
}

function transformOriginalArrayToFilteredArray(array) {
  return array.filter((el, index) => indexes.indexOf(index) >= 0)
}

_(stream)
  .split()
  .drop(6)
  .filter(filterEmptyLine)
  .map(transformStringToArray)
  .map(transformOriginalArrayToFilteredArray)
  .each(value => console.log(value))
